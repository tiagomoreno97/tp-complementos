import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import java.awt.CardLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JLayeredPane;

public class Campeonato extends JFrame {

	/**
	 * @author tiago
	 * 
	 * classe para fazer o login
	 */
	
	private JPanel contentPane;
	private JTextField username;
	private JPasswordField password;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Campeonato frame = new Campeonato();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Campeonato() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		JPanel Login = new JPanel();
		contentPane.add(Login, "name_8112778403470");
		Login.setLayout(null);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(42, 40, 72, 14);
		Login.add(lblUsername);
		
		username = new JTextField();
		username.setBounds(108, 37, 111, 20);
		Login.add(username);
		username.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(42, 79, 72, 14);
		Login.add(lblPassword);
		
		password = new JPasswordField();
		password.setBounds(108, 79, 111, 20);
		Login.add(password);
		
		/**
		 * vai receber o nome do utilizador  a passe e vai comparar se for iguais vai para frame tabela
		 * @param nome
		 * @param pwd
		 * @return tabela
		 */
		JButton btnLogin = new JButton("Entrar");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String uname= username.getText();
				String pwd= password.getText();
				
				if(uname.equals("tiago") && pwd.equals("pass") ){
					
					JOptionPane.showMessageDialog(Login, "Login com sucesso");
					
					tabela tabela = null;
					try {
						tabela = new tabela();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					tabela.setVisible(true);
					
					
					
				}else{
					JOptionPane.showMessageDialog(Login, "username ou password est�ｿｽ�ｽｾ�ｿｽ�ｽｽ�ｽ｣o errados");
				}
			}
		});
		btnLogin.setBounds(237, 164, 89, 23);
		Login.add(btnLogin);
	}
}

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.*;
import javax.swing.*;

/**
 * @author tiago
 * 
 * classe para mostrar o plantel das equipas
 */

public class plantel extends JFrame {

	Connection connection =null;
	private JPanel contentPane;
	private JTable table;
	private JComboBox comboBox;
	

	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					plantel frame = new plantel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * 
	 * vai preencher a combobox
	 * @param query
	 * @param return
	 * @param combobox
	 * 
	 * 
	 */
public void fillcombobox(){
		
		try{
			String query=" select * from Equipa";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				comboBox.addItem(rs.getString("nomeEquipa"));
				
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		
	}

/**
 * 
 * vai ver se o valor da combobox e igual ao nome de equipa  se for verdade mostra a tabela equipa
 * 
 * @param combobox
 * @param return
 * @param tabela
 * 
 * 
 */
public void table_equipa(){

	if(comboBox.getSelectedItem().equals("Benfica")){		
	
		try{
		
	String query="Select * from Jogadores Where idEquipa = 1";
	PreparedStatement pst=connection.prepareStatement(query);
	ResultSet rs=pst.executeQuery();
	table.setModel(DbUtils.resultSetToTableModel(rs));
		
		
			}catch(Exception e){
		JOptionPane.showMessageDialog(null,e);
	}
}if(comboBox.getSelectedItem().equals("Porto")){	
	
	try{
		
		String query="Select * from Jogadores Where idEquipa = 2";
		PreparedStatement pst=connection.prepareStatement(query);
		ResultSet rs=pst.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
			
			
				}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
	
	
	
}if(comboBox.getSelectedItem().equals("Sporting")){	
	
	try{
		
		String query="Select * from Jogadores Where idEquipa = 3";
		PreparedStatement pst=connection.prepareStatement(query);
		ResultSet rs=pst.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
			
			
				}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
	
	
	
}if(comboBox.getSelectedItem().equals("Braga")){	
	
	try{
		
		String query="Select * from Jogadores Where idEquipa = 4";
		PreparedStatement pst=connection.prepareStatement(query);
		ResultSet rs=pst.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
			
			
				}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
	
	
	
}if(comboBox.getSelectedItem().equals("Arouca")){	
	
	try{
		
		String query="Select * from Jogadores Where idEquipa = 5";
		PreparedStatement pst=connection.prepareStatement(query);
		ResultSet rs=pst.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
			
			
				}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
	
	
	
}
		
	
	
}	
		
		



	/**
	 * Create the frame.
	 */
	public plantel() {
		connection=sqliteConnection.dbConnector();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 620, 522);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(20, 11, 574, 461);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblPlantel = new JLabel("Plantel");
		lblPlantel.setBounds(134, 63, 46, 14);
		panel.add(lblPlantel);
		
		comboBox = new JComboBox();
		comboBox.setBounds(229, 60, 123, 20);
		panel.add(comboBox);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 160, 554, 246);
		panel.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnMostrar = new JButton("Mostrar");
		btnMostrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				table_equipa();
				
			}
		});
		btnMostrar.setBounds(404, 59, 89, 23);
		panel.add(btnMostrar);
		
		fillcombobox();
		
	}

}

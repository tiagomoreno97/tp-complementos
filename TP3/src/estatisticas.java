import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JScrollPane;

public class estatisticas extends JFrame {
	Connection connection =null;
	private JPanel contentPane;
	private JTable table;
	private JComboBox comboBox;
	private JComboBox comboBox_1;
	/**
	 * Launch the application.
	 */
	
	
	/**
	 * 
	 * @author tiago
	 * 
	 * mostra as estatisticas das equipas/jogadores
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					estatisticas frame = new estatisticas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * 
	 * vai ver se o valor da combobox e igual 17/18 se for verdade mostra a tabela das estatisticas por epoca
	 * 
	 * @param combobox
	 * @param return
	 * @param tabela
	 * 
	 * 
	 */
	
	public void table_EstGeral2(){

		if(comboBox.getSelectedItem().equals("17/18")){		
		
			try{
			
		String query="Select * from geral where idGeral = 2 ";
		PreparedStatement pst=connection.prepareStatement(query);
		ResultSet rs=pst.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
			
			
				}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
		}
		}
	
	/**
	 * 
	 * vai ver se o valor da combobox e igual 16/17 se for verdade mostra a tabela das estatisticas por epoca
	 * 
	 * @param combobox
	 * @param return
	 * @param tabela
	 * 
	 * 
	 */
	
	public void table_EstGeral(){

		if(comboBox.getSelectedItem().equals("16/17")){		
		
			try{
			
		String query="Select * from geral where idGeral = 1 ";
		PreparedStatement pst=connection.prepareStatement(query);
		ResultSet rs=pst.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
			
			
				}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
		}
		}
	
	
	/**
	 * 
	 * vai ver se o valor da combobox e igual 17/18 ou a equipa selecionada se for verdade mostra a tabela das estatisticas por equipa
	 * 
	 * @param combobox
	 * @param return
	 * @param tabela
	 * 
	 * 
	 */
	public void table_EstEquipaT2(){

		if(comboBox_1.getSelectedItem().equals("Benfica") && comboBox.getSelectedItem().equals("17/18") ){		
		
			try{
			
		String query="Select * from EstEquipaT2 Where idEstEquipa = 1";
		PreparedStatement pst=connection.prepareStatement(query);
		ResultSet rs=pst.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
			
			
				}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
	}if(comboBox_1.getSelectedItem().equals("Porto")&& comboBox.getSelectedItem().equals("17/18")){	
		
		try{
			
			String query="Select * from EstEquipaT2 Where idEstEquipa = 2";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}if(comboBox_1.getSelectedItem().equals("Sporting")&& comboBox.getSelectedItem().equals("17/18") ){	
		
		try{
			
			String query="Select * from EstEquipaT2 Where idEstEquipa = 3";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}if(comboBox_1.getSelectedItem().equals("Braga")&& comboBox.getSelectedItem().equals("17/18")){	
		
		try{
			
			String query="Select * from EstEquipaT2 Where idEstEquipa = 4";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}if(comboBox_1.getSelectedItem().equals("Arouca")&& comboBox.getSelectedItem().equals("17/18")){	
		
		try{
			
			String query="Select * from EstEquipaT2 Where idEstEquipa = 5";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}
			

	/**
	 * 
	 * vai ver se o valor da combobox e igual 16/17 ou a equipa selecionada se for verdade mostra a tabela das estatisticas por equipa
	 * 
	 * @param combobox
	 * @param return
	 * @param tabela
	 * 
	 * 
	 */
		
	}	
	
	
	
	public void table_EstEquipa(){

		if(comboBox_1.getSelectedItem().equals("Benfica") && comboBox.getSelectedItem().equals("16/17") ){		
		
			try{
			
		String query="Select * from EstEquipa Where idEstEquipa = 1";
		PreparedStatement pst=connection.prepareStatement(query);
		ResultSet rs=pst.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
			
			
				}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
	}if(comboBox_1.getSelectedItem().equals("Porto")&& comboBox.getSelectedItem().equals("16/17")){	
		
		try{
			
			String query="Select * from EstEquipa Where idEstEquipa = 2";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}if(comboBox_1.getSelectedItem().equals("Sporting")&& comboBox.getSelectedItem().equals("16/17") ){	
		
		try{
			
			String query="Select * from EstEquipa Where idEstEquipa = 3";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}if(comboBox_1.getSelectedItem().equals("Braga")&& comboBox.getSelectedItem().equals("16/17")){	
		
		try{
			
			String query="Select * from EstEquipa Where idEstEquipa = 4";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}if(comboBox_1.getSelectedItem().equals("Arouca")&& comboBox.getSelectedItem().equals("16/17")){	
		
		try{
			
			String query="Select * from EstEquipa Where idEstEquipa = 5";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}
			
		

	/**
	 * 
	 * vai ver se o valor da combobox e igual 16/17 ou a equipa selecionada se for verdade mostra a tabela das estatisticas por jogador
	 * 
	 * @param combobox
	 * @param return
	 * @param tabela
	 * 
	 * 
	 */
	
	}
	
	
	public void table_EstJogador(){

		if(comboBox_1.getSelectedItem().equals("Benfica") && comboBox.getSelectedItem().equals("16/17") ){		
		
			try{
			
		String query="Select * from EstJogador Where idEquipa = 1";
		PreparedStatement pst=connection.prepareStatement(query);
		ResultSet rs=pst.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
			
			
				}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
	}if(comboBox_1.getSelectedItem().equals("Porto")&& comboBox.getSelectedItem().equals("16/17")){	
		
		try{
			
			String query="Select * from EstJogador Where idEquipa = 2";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}if(comboBox_1.getSelectedItem().equals("Sporting")&& comboBox.getSelectedItem().equals("16/17") ){	
		
		try{
			
			String query="Select * from EstJogador Where idEquipa = 3";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}if(comboBox_1.getSelectedItem().equals("Braga")&& comboBox.getSelectedItem().equals("16/17")){	
		
		try{
			
			String query="Select * from EstJogador Where idEquipa = 4";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}if(comboBox_1.getSelectedItem().equals("Arouca")&& comboBox.getSelectedItem().equals("16/17")){	
		
		try{
			
			String query="Select * from EstJogador Where idEquipa = 5";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}
			
		
	/**
	 * 
	 * vai ver se o valor da combobox e igual 17/18 ou a equipa selecionada se for verdade mostra a tabela das estatisticas por jogador
	 * 
	 * @param combobox
	 * @param return
	 * @param tabela
	 * 
	 * 
	 */
		
	}
	public void table_EstJogadorT2(){

		if(comboBox_1.getSelectedItem().equals("Benfica") && comboBox.getSelectedItem().equals("17/18") ){		
		
			try{
			
		String query="Select * from EstJogadorT2 Where idEquipa = 1";
		PreparedStatement pst=connection.prepareStatement(query);
		ResultSet rs=pst.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
			
			
				}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
	}if(comboBox_1.getSelectedItem().equals("Porto")&& comboBox.getSelectedItem().equals("17/18")){	
		
		try{
			
			String query="Select * from EstJogadorT2 Where idEquipa = 2";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}if(comboBox_1.getSelectedItem().equals("Sporting")&& comboBox.getSelectedItem().equals("17/18") ){	
		
		try{
			
			String query="Select * from EstJogadorT2 Where idEquipa = 3";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}if(comboBox_1.getSelectedItem().equals("Braga")&& comboBox.getSelectedItem().equals("17/18")){	
		
		try{
			
			String query="Select * from EstJogadorT2 Where idEquipa = 4";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}if(comboBox_1.getSelectedItem().equals("Arouca")&& comboBox.getSelectedItem().equals("17/18")){	
		
		try{
			
			String query="Select * from EstJogadorT2 Where idEquipa = 5";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
				
				
					}catch(Exception e){
				JOptionPane.showMessageDialog(null,e);
			}
		
		
		
	}
			
		
		
	}
	/**
	 * 
	 * vai preencher a combobox
	 * @param query
	 * @param return
	 * @param combobox
	 * 
	 * 
	 */
	
public void fillcombobox(){
		
		try{
			String query=" select * from epoca";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				comboBox.addItem(rs.getString("dataEpoca"));
				
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		
	}
	
/**
 * 
 * vai preencher a combobox
 * @param query
 * @param return
 * @param combobox
 * 
 * 
 */
	
public void fillcombobox1(){
	
	try{
		String query=" select * from Equipa";
		PreparedStatement pst=connection.prepareStatement(query);
		ResultSet rs=pst.executeQuery();
		
		while(rs.next()){
			comboBox_1.addItem(rs.getString("nomeEquipa"));
			
		}
	}catch (Exception e){
		e.printStackTrace();
	}
	
}

	

	/**
	 * Create the frame.
	 */
	public estatisticas() {
		connection=sqliteConnection.dbConnector();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1271, 486);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(20, 11, 1225, 403);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblpocas = new JLabel("\u00C9pocas");
		lblpocas.setBounds(295, 35, 54, 14);
		panel.add(lblpocas);
		
		comboBox = new JComboBox();
		comboBox.setBounds(359, 32, 122, 20);
		panel.add(comboBox);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(31, 146, 1184, 87);
		panel.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnNewButton = new JButton("Geral");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				table_EstGeral();
				table_EstGeral2();
			}
		});
		btnNewButton.setBounds(185, 98, 89, 23);
		panel.add(btnNewButton);
		
		JButton btnEquipas = new JButton("Equipas");
		btnEquipas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				table_EstEquipa();
				table_EstEquipaT2();
			}
		});
		btnEquipas.setBounds(556, 98, 89, 23);
		panel.add(btnEquipas);
		
		JButton btnJogador = new JButton("Jogador");
		btnJogador.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				table_EstJogador();
				table_EstJogadorT2();
			}
		});
		btnJogador.setBounds(926, 98, 89, 23);
		panel.add(btnJogador);
		
		JLabel lblEquipas = new JLabel("Equipas");
		lblEquipas.setBounds(674, 35, 60, 14);
		panel.add(lblEquipas);
		
		comboBox_1 = new JComboBox();
		comboBox_1.setBounds(744, 32, 122, 20);
		panel.add(comboBox_1);
		
		fillcombobox();
		fillcombobox1();
		
	}
}

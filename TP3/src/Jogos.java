import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JScrollPane;
/**
 * @author tiago
 * 
 * classe para mostrar os jogos
 */

public class Jogos extends JFrame {
	
	Connection connection =null;
	private JPanel contentPane;
	private JTable table;
	private JComboBox comboBox;
	private JButton btnMostrar;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Jogos frame = new Jogos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * 
	 * vai preencher a combobox
	 * @param query
	 * @param return
	 * @param combobox
	 * 
	 * 
	 */

public void fillcombobox(){
		
		try{
			String query=" select * from epoca";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				comboBox.addItem(rs.getString("dataEpoca"));
				
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		
	}
/**
 * 
 * vai ver se o valor da combobox e igual 17/18 se for verdade mostra a tabela dos jogos
 * 
 * @param combobox
 * @param return
 * @param tabela
 * 
 * 
 */

public void table_jogosT2(){

	if(comboBox.getSelectedItem().equals("17/18")){		
	
		try{
		
	String query="Select * from JogosT2 ";
	PreparedStatement pst=connection.prepareStatement(query);
	ResultSet rs=pst.executeQuery();
	table.setModel(DbUtils.resultSetToTableModel(rs));
		
		
			}catch(Exception e){
		JOptionPane.showMessageDialog(null,e);
	}
	}
	}
/**
 * 
 * vai ver se o valor da combobox e igual 17/18 se for verdade mostra a tabela dos jogos
 * 
 * @param combobox
 * @param return
 * @param tabela
 * 
 * 
 */

	
public void table_jogos(){

	if(comboBox.getSelectedItem().equals("16/17")){		
	
		try{
		
	String query="Select * from Jogos ";
	PreparedStatement pst=connection.prepareStatement(query);
	ResultSet rs=pst.executeQuery();
	table.setModel(DbUtils.resultSetToTableModel(rs));
		
		
			}catch(Exception e){
		JOptionPane.showMessageDialog(null,e);
	}
	}
	}
	
	/**
	 * Create the frame.
	 */
	public Jogos() {
		connection=sqliteConnection.dbConnector();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 641, 426);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 605, 365);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblpoca = new JLabel("\u00C9poca");
		lblpoca.setBounds(186, 61, 46, 14);
		panel.add(lblpoca);
		
	    comboBox = new JComboBox();
		comboBox.setBounds(242, 58, 132, 20);
		panel.add(comboBox);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 139, 585, 215);
		panel.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		btnMostrar = new JButton("Mostrar");
		btnMostrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				table_jogos();
				table_jogosT2();
				
			}
		});
		btnMostrar.setBounds(445, 57, 89, 23);
		panel.add(btnMostrar);
		
		fillcombobox();
		
		
	}
}

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;
import javax.swing.*;
/**
 * @author tiago
 * 
 * classe para mostrar as classificacoes da equipa
 */

public class tabela extends JFrame {

	
	Connection connection =null;
	private JPanel contentPane;
	private JTable table;
	private JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	
	
	
	
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					tabela frame = new tabela();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * 
	 * vai ver se o valor da combobox e igual a 17/18  se for verdade mostra a tabela classificacao2
	 * 
	 * @param combobox
	 * @param return
	 * @param tabela
	 * 
	 * 
	 */
	
	public void table_classi2(){
		if(comboBox.getSelectedItem().equals("17/18")){
		try{
			
		
		String query=" select * from Classificacao2 ORDER BY Pontos DESC";
		PreparedStatement pst=connection.prepareStatement(query);
		ResultSet rs=pst.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
		
		}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
		
	}
	}
	
	/**
	 * 
	 * vai ver se o valor da combobox e igual a 16/17  se for verdade mostra a tabela classificacao
	 * 
	 * @param combobox
	 * @param return
	 * @param tabela
	 * 
	 * 
	 */
	
	
	public void updatetable(){
		if(comboBox.getSelectedItem().equals("16/17")){
		try{
			
		
		String query=" select * from Classificacao ORDER BY Pontos DESC";
		PreparedStatement pst=connection.prepareStatement(query);
		ResultSet rs=pst.executeQuery();
		table.setModel(DbUtils.resultSetToTableModel(rs));
		
		}catch(Exception e){
			JOptionPane.showMessageDialog(null,e);
		}
		
	}
	}
	/**
	 * 
	 * vai preencher a combobox
	 * @param query
	 * @param return
	 * @param combobox
	 * 
	 * 
	 */
	public void fillcombobox(){
		
		try{
			String query=" select * from epoca";
			PreparedStatement pst=connection.prepareStatement(query);
			ResultSet rs=pst.executeQuery();
			
			while(rs.next()){
				comboBox.addItem(rs.getString("dataEpoca"));
				
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public tabela() throws SQLException {
		
		       
		connection=sqliteConnection.dbConnector();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 607, 594);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel classificacao = new JPanel();
		classificacao.setBounds(10, 11, 571, 533);
		contentPane.add(classificacao);
		classificacao.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u00C9pocas");
		lblNewLabel.setBounds(166, 36, 46, 14);
		classificacao.add(lblNewLabel);
		
		 comboBox = new JComboBox();
		 comboBox.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 		updatetable();
		 		table_classi2();
		 	}
		 });
	
		comboBox.setBounds(239, 33, 122, 20);
		classificacao.add(comboBox);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(23, 141, 538, 249);
		classificacao.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnEstatisticas = new JButton("Estatisticas");
		btnEstatisticas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				estatisticas estatisticas = new estatisticas();
				estatisticas.setVisible(true);
				
			}
		});
		btnEstatisticas.setBounds(46, 87, 108, 23);
		classificacao.add(btnEstatisticas);
		
		JButton btnPlantel = new JButton("Plantel");
		btnPlantel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				plantel plantel =new plantel();
				plantel.setVisible(true);
			}
		});
		btnPlantel.setBounds(257, 87, 89, 23);
		classificacao.add(btnPlantel);
		
		JButton btnJogos = new JButton("Jogos");
		btnJogos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Jogos Jogos= new Jogos();
				Jogos.setVisible(true);
			}
		});
		btnJogos.setBounds(427, 87, 89, 23);
		classificacao.add(btnJogos);
		
		
		fillcombobox();
		
	}
}
